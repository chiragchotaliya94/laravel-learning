<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("login");
});

Route::get("admin/dashboard", 'AdminController@dashboard');
Route::get("user/create", "UserController@create");
Route::post("user/save","UserController@save");
Route::get("users", "UserController@index");
Route::get("user/delete/{id}", 'UserController@delete');
Route::get("user/details/{id}","UserController@find");
Route::get("user/edit/{id}","UserController@edit");
Route::post("user/update","UserController@update");

Route::get('create/account', 'SignUpController@create');
Route::post("create/account/save","SignUpController@save");
Route::get("login", "LoginController@login");
Route::post('/loginprocess','LoginController@loginprocess');
Route::get('/logout', "LoginController@logout");


