<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    function dashboard()
    {

        $users=DB::table("users")->get()->count();

        return view('admin.dashboard',[
            'users' => $users
        ]);
    }
}
