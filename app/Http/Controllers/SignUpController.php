<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SignUpController extends Controller
{
    function create()
    {
        return view('signUp.create');
    }

    function save(Request $req)
    {
        $req->validate([
            'username' => ['required', 'min:8', 'max:15'],
            'email' => ['required'],
            'password'=>['required','min:3','max:8'],
            ]);

            DB::table("admin")
            ->insert([
                'username'=>$req->input("username"),
                'email'=>$req->input("email"),
                'password'=> $req->input("password")
            
            ]);

            return redirect('login');

    }
}
