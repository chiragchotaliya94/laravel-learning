<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function create()
    {

        if(session()->has('data'))
        {
            return view('user.create');
        }
        else
        {
            return redirect("login");
        }
       
    }
    public function save(Request $req)
    {
        if(!session()->has('data'))
        {
            return redirect("login");
        }
        $req->validate([
            'firstname'=> ['required'],
            'lastname'=> ['required'],
            'username' => ['required', 'min:8', 'max:15'],
            'email' => ['required'],
            'password'=>['required','min:3','max:8'],
            ]);

        DB::table("users")
            ->insert([
                'firstname'=>$req->input("firstname"),
                'lastname'=>$req->input("lastname"),
                'username'=>$req->input("username"),
                'email'=>$req->input("email"),
                'password'=>$req->input("password")
            
            ]);

        $req->session()->flash('flash_message', 'User Added SuccessFully');
        return redirect("users");
    }
    public function index()
    {
        if(session()->has('data'))
        {
            $users=DB::table("users")->get();

            return view("user.index",['users'=>$users]);

            return view("users");
        }
        else
        {
            return redirect("login");
        }
    }

    public function delete($id){

        if(session()->has('data'))
        {
            DB::table('users')
            ->delete($id);

            session()->flash('flash_message', 'User Delete SuccessFully');

            return redirect("users");
        }
        else
        {
            return redirect("login");
        }
 }

    function find($id)
    {

        if(session()->has('data')){

            $user=DB::table("users")->find($id);
            return view("user.details",['user'=>$user]);
        }
        else{
            return redirect("login");
        }
    }

    function edit($id)
    {
        if(session()->has('data')){
            $user=DB::table("users")->find($id);

            return view("user.edit",['user'=>$user]);
        }
        else{
                return redirect("login");
        }
    }


    function update(Request $req)
    {
        if(session()->has("data"))
        {
            $req->validate([
                'firstname'=> ['required'],
                'lastname'=> ['required'],
                'username' => ['required', 'min:8', 'max:15'],
                'email' => ['required'],
                
                ]);
            $id = $req->request->get('id');
            $user=DB::table("users")->find($id);

            $oldPassword = $user->password;
            
            if(empty($req->input('password')))
            {
                $newpassword = $oldPassword;
            }
            else{
                $newpassword = $req->input('password');
            }
            
            DB::table("users")
                ->where("id",$req->input("id"))
                ->update([
                    'firstname'=>$req->input("firstname"),
                    'lastname'=>$req->input("lastname"),
                    'username'=>$req->input("username"),
                    'email'=>$req->input("email"),
                    'password' => $newpassword,
                ]);
                
                $req->session()->flash('flash_message', 'User Update Successfully');

    
            return redirect("users");
        }
        else{
            return redirect("login");
        }
    }

}