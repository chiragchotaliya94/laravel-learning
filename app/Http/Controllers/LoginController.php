<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    function login()
    {

        return view('login.login');
    }

    function loginprocess(Request $req)
    {   
        $validator = $req->validate([
            'username'     => 'required',
            'password'  => 'required|min:6'
        ]);
        
        $username = $req->request->get("username");
        $password = $req->request->get("password");

        $result = DB::table('admin')->where([
            'username' => $username,
            'password' => $password
        ])->first();

        if(!empty($result))
        {
            $req->session()->put('data', $req->input());
        
            return redirect("/admin/dashboard");

        }
        else{
            echo "UserName Or Password  is Worng";

            return redirect('login');
        }
    }

    function logout()
    {
        session()->forget('data');

        return redirect("login");
    }
}