<!DOCTYPE html>
<html lang="en">
    @include('admin.login.header');
<body>
<div class="login-form">
    <form method="post" action="/loginprocess">
        @csrf
        <h2 class="text-center"> Admin Log in</h2>       
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Enter UserName" name="username" value="">
                @error('username')
                <p style="color: red">{{$message}}</p>
                 @enderror
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Enter Password" name="password" value="">
                @error('password')
                <p style="color: red">{{$message}}</p>
                 @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" value="Log in">
        </div>        
    </form>
    <p class="text-center"><a href="/create/account">Create an Account</a></p>
</div>
</body>
</html>