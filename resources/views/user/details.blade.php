<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
</head>
<body>

<table border="1" cellspacing="1" cellpadding="4"  align="center">
    <tr align="center" bgcolor="skyblue">
        <td colspan="2">Single user Information</td>
    </tr>    
    <tr>
        <td>First Name :: </td>
        <td>{{$user->firstname}}</td>
    </tr>

    <tr>
        <td>Last Name :: </td>
        <td>{{$user->lastname}}</td>
    </tr>

    <tr>
        <td>UserName:: </td>
        <td>{{$user->username}}</td>
    </tr>

    <tr>
        <td>Email :: </td>
        <td>{{$user->email}}</td>
    </tr>

    <tr>
        <td colspan="2" bgcolor="yellow" align="center">
            <a href="/users">Back to User List</a>
        </td>
    </tr>
</table>

</body>
</html>
