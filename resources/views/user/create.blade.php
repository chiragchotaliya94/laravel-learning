@extends("admin.dashboard");

@section('mainSection')

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Create User</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post", action="/user/save">
                @csrf
              <div class="card-body">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" name="firstname" class="form-control" placeholder="Enter FirstName">
                  @error('firstname')
                    <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="lastname" class="form-control" placeholder="Enter LastName">
                  @error('lastname')
                    <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" name="username" class="form-control" placeholder="Enter UserName">
                    @error('username')
                        <p style="color: red">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter Email">
                    @error('email')
                  <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Enter Password">
                    @error('password')
                        <p style="color: red">{{$message}}</p>
                    @enderror
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection

