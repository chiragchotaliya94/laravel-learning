{{--  <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Update User</title>
</head>
<body>

    <table border="1" cellspacing="1" cellpadding="4" align="center">
    <tr>
        <td colspan="2" bgcolor="skyblue" align="center"> Update User</td>
    </tr>   
    <form method="post" action="/user/update">
        @csrf

        <tr>
            <input type="hidden" name="id" value="{{$user->id}}">
            <td>First Name:: </td>
            <td>
            <input type="text" name="firstname" value="{{$user->firstname}}"> <br>
            @error('firstname')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>Last Name :: </td>
            <td>
                <input type="text" name="lastname" value="{{$user->lastname}}">
                @error('lastname')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>User Name :: </td>
            <td>
                <input type="text" name="username" value="{{$user->username}}">

                @error('username')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>Email :: </td>
            <td>
                <input type="email" name="email" value="{{$user->email}}">
                @error('email')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>Password :: </td>
            <td>
                <input type="password" name="password" value="">
            </td>
        </tr>

        <tr>
            <td colspan="2" bgcolor="skyblue" align="center">
                <input type="submit" name="btnSave" value="Save">
            </td>
            
        </tr>

        <tr>
            <td colspan="2" bgcolor="yellow" align="center">
                <a href="/users">Back to User List</a>
            </td>
        </tr>
    </form>
</table>

</body>
</html>  --}}

@extends("admin.dashboard");

@section('mainSection')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Update User</small></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="post", action="/user/update">
                @csrf
              <div class="card-body">
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="form-group">
                  <label>First Name</label>
                <input type="text" name="firstname" class="form-control" placeholder="Enter FirstName" value="{{$user->firstname}}">
                  @error('firstname')
                    <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" name="lastname" class="form-control" placeholder="Enter LastName" value="{{$user->lastname}}">
                  @error('lastname')
                    <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" name="username" class="form-control" placeholder="Enter UserName" value="{{$user->username}}">
                    @error('username')
                        <p style="color: red">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{$user->email}}">
                    @error('email')
                  <p style="color: red">{{$message}}</p>
                  @enderror
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Enter Password" value="">
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="/users" class="btn btn-danger">Cancle</a>
              </div>
            </form>
          </div>
          <!-- /.card -->
          </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
@endsection
