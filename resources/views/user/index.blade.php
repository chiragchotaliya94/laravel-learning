@extends('admin.dashboard')
@section('mainSection')
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
         <div class="card">
            <div class="card-header">
              <h3 class="card-title">List Of All User</h3> 
            </div>    
            @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
            @endif
            <!-- /.card-header -->
            <div class="card-body">
              <table id="users" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                            <td>{{$user->firstname}}</td>
                                <td>{{$user->lastname}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="/user/edit/{{$user->id}}"" class="btn btn-success">Edit</a>
                                    <a onclick="return confirm('Are you sure to delete this user?')" href="/user/delete/{{$user->id}}" class="btn btn-danger">Delete</a>
                                    {{--  <a href="" class="btn btn-info">Show</a>  --}}
                                </td>
                            </tr>   
                        @endforeach
                    </tbody>
              </table>
              <a href="/user/create" class="btn btn-success">User Create</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>f
@endsection
p