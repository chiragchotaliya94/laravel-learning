<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
    <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
    <span class="brand-text font-weight-light">Welcome
      @if(!empty(session()->get('data')))
      {{session()->get('data')['username']}}
      @else
      {{"Admin"}}    
      @endif
  </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      @php 
      $currentRoute = Request::route()->uri;
      
      @endphp
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Chirag Chotaliya</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item has-treeview {{ Request::path() ==  'admin/dashboard' ? 'menu-open' : ''  }} ">
            <a href="#" class="nav-link {{ Request::path() ==  'admin/dashboard' ? 'active' : ''  }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/admin/dashboard" class="nav-link {{ Request::path() ==  'admin/dashboard' ? 'active' : ''  }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
            </ul>
        </li>
        <li class="nav-item has-treeview" @if($currentRoute == "user/create" || $currentRoute == "users") class="menu-open" @endif>
                <a href="#" class="nav-link" @if($currentRoute == 'user/create' || $currentRoute == "users") class="active" @endif>
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    User
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/user/create" class="nav-link  {{ Request::path() ==  'user/create' ? 'active' : ''  }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Create</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/users" class="nav-link {{ Request::path() ==  'users' ? 'active' : ''  }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>List</p>
                    </a>
                  </li>
                </ul>
            </li>
          </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
