{{--  <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create Account</title>
</head>
<body>

    <table border="1" cellspacing="1" cellpadding="4" align="center">
    <tr>
        <td colspan="2" bgcolor="skyblue" align="center"> Create Account</td>
    </tr>   
    <form method="post" action="/create/account/save">
        @csrf
        <tr>
            <td>User Name :: </td>
            <td>
                <input type="text" name="username" value="">
                @error('username')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>Email :: </td>
            <td>
                <input type="email" name="email" value="">
                @error('email')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td>Password :: </td>
            <td>
                <input type="password" name="password" value="">
                @error('password')
                <p style="color: red">{{$message}}</p>
                 @enderror
            </td>
        </tr>

        <tr>
            <td colspan="2" bgcolor="skyblue" align="center">
                <input type="submit" name="btnSave" value="Save">
            </td>
            
        </tr>

        <tr>
            <td colspan="2" bgcolor="pink" align="center">
                <a href="/login">Back to Login</a>
            </td>
            
        </tr>
    </form>
</table>

</body>
</html>  --}}

<!DOCTYPE html>
<html lang="en">
    @include('admin.login.header');
    @section('title','Create Account')
<body>
<div class="login-form">
    <form method="post" action="/create/account/save">
        @csrf
        <h2 class="text-center">Create Account</h2>       
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Enter UserName" name="username" value="">
                @error('username')
                <p style="color: red">{{$message}}</p>
                 @enderror
        </div>

        <div class="form-group">
            <input type="email" class="form-control" placeholder="Enter Email" name="email" value="">
                @error('email')
                <p style="color: red">{{$message}}</p>
                 @enderror
        </div>

        <div class="form-group">
            <input type="password" class="form-control" placeholder="Enter Password" name="password" value="">
                @error('password')
                <p style="color: red">{{$message}}</p>
                 @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary btn-block" value="Log in">
        </div>        
    </form>
</div>
</body>
</html>
